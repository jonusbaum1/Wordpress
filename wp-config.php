<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'test');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost:8889');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'doE~Q]5d_]j8MGKqw_Vyk$<a?ZkW!Pb_@g2FJ[K8}_K5<bd21uT2L,ZSXtw-:*a?');
define('SECURE_AUTH_KEY',  '|N}Z-^H/lnJCJ)JQFsJ^s q4N~@2Y<dA]};<qC%[2x/z6^vi{bXQ$at3Iwb{dwx[');
define('LOGGED_IN_KEY',    'NLC-o ^lVI4<`byk$Tt4~c&OU?~QC&lhrD|c<LrIr)cGdUf|)o4r+{Zi}x{O*F$2');
define('NONCE_KEY',        '7!QaK9-d@$;oXNQn-[K=6}bjZ^o9)o(ZA6+&$:x,vQ#e9==iwx+?F8Z=y{K4847L');
define('AUTH_SALT',        'xAUS,6r::[,b?8r.KR=f//`>rp=B!um$ta3_;rb0Bb.d4?WlTgnM]+UQc@v&ze/f');
define('SECURE_AUTH_SALT', 'f1+f{}(!`RF$Hg@K9_5$M)mnnli<]xVmlW $6xg(4G~RA5zJN=f1}9K&3glOt6X4');
define('LOGGED_IN_SALT',   'MV@<.443x%)QHyR:C?M[mP>|HrgRIOr[?rx[-eMRdbW! 7lAc=9rhmQZX41=t?Vd');
define('NONCE_SALT',       '3hbZSAvs{T2 b.wd.y#v/K.1DE`k.P@9@TBdT<]C2 ulvy-/6B.f4[/|;8 v_4V`');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');