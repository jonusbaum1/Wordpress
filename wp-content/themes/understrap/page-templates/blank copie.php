<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<body>
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'loop-templates/content', 'blank' ); ?>

<?php endwhile; // end of the loop. ?>
<div id="fullpage">
	<div class="section " id="section0">
		<div class="slide" id="slide1">
		<img class="barre" width="1px" src="http://localhost/wordpress/wp-content/themes/understrap-child/img/barre.svg">
			<span class="titre align-middle">FROM JAMAICA TO THE WORLD !!</span>
			<img class="barre" width="1px" src="http://localhost/wordpress/wp-content/themes/understrap-child/img/barre.svg">
		</div>
		<div class="slide" id="slide2"><span class="titre align-middle">PROJECT TAB FOR CULINARY ROOTS</span></div>
	</div>

	<div class="section" id="section1">
	  	<div class="container fill">
    		<div id="map">
    			<div class="row">
		        	<div class="col-md-4 projet1">ss</div>
		        	<div class="col-md-4 projet2">ss</div>
		        	<div class="col-md-4 projet3">ss</div>
	    		</div>
	    	</div> <!-- This one wants to be 100% height -->
  		</div>
  	</div>
	
	<div class="section" id="section2">
		<div class="intro">
			<h1>Keep it simple!</h1>
		</div>
	</div>    	
</div>



<?php wp_footer(); ?>
</body>
</html>
