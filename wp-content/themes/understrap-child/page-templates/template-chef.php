<?php
/**
 * Template Name: CHEF
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<!--*FULLPAGE OPTIONT*-->	
<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors: ['firstPage', 'secondPage', '3rdPage', '4th page' ],
			sectionsColor: ['#transparent', 'transparent', 'transparent'],
			navigation: false,
			navigationPosition: 'right',
			navigationTooltips: ['First page', 'Second page', 'Third and last page', '4th page'],
            scrollOverflow: true,
			afterResponsive: function(isResponsive){

			}

		});
	});
</script>

<!--*END FULLPAGE OPTIONT*-->
<body>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'loop-templates/content', 'blank' ); ?>

	<?php endwhile; // end of the loop. ?>

<div id="fullpage">

	<!-- SECTION 0  -->
	<div class="section" id="section">
		<div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('image_section_0'); ?>') no-repeat; background-size:cover;background-position: center top;">
                </div>	

                <div class="col-12 col-md-8">
                    <div class="bio">
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_0'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_0'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_0'); ?></p>
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
	</div><!-- SECTION 0  -->

	<div class="section" id="section1">
		<div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('image_section_1'); ?>') no-repeat; background-size:cover;background-position: center top;">
                </div>	

                <div class="col-12 col-md-8">
                    <div class="bio">				
                    		<div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_1'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_1'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_1'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
	</div>	<!-- //section -->

</div><!-- //fullpage -->



<?php wp_footer(); ?>
</body>
</html>
