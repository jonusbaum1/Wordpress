<?php
/**
 * Template Name: ACADEMIA
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<!--*FULLPAGE OPTIONT*-->	
<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors: ['1st Page', '2nd Page', '3rdPage', '4th page', '5th Page', '6th Page', '7th Page', '8th Page' ],
			sectionsColor: ['#transparent', 'transparent', 'transparent'],
			navigation: false,
			navigationPosition: 'right',
			navigationTooltips: ['1st Page', '2nd Page', '3rdPage', '4th page', '5th Page', '6th Page', '7th Page', '8th Page'],
            scrollOverflow: false,
			afterResponsive: function(isResponsive){

			}

		});
	});
</script>

<!--*END FULLPAGE OPTIONT*-->
<body>
	<?php while ( have_posts() ) : the_post(); ?>


	<?php endwhile; // end of the loop. ?>
<div id="fullpage">

	<!-- SECTION 1  -->
	<div class="section" id="section1">
		<div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;">
                </div>	

                <div class="col-12 col-md-8">
                    <div class="bio">
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_1'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_1'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_1'); ?></p>
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
	</div>
    <!-- SECTION 2  -->

	<div class="section" id="section2">
		<div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">				
                    		<div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_2'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_2'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_2'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
	</div>	<!-- //section -->
    <!-- SECTION 3  -->

    <div class="section" id="section3">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_3'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_3'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_3'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
    <!-- SECTION 4  -->

    <div class="section" id="section4">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_4'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_4'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_4'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
    <!-- SECTION 5  -->

    <div class="section" id="section5">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_5'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_5'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_5'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
    <!-- SECTION 6  -->

    <div class="section" id="section6">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_6'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_6'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_6'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
    <!-- SECTION 7  -->

    <div class="section" id="section7">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_7'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_7'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_7'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
    <!-- SECTION 8  -->

    <div class="section" id="section8">
        <div class="container-fluid h-100 d-flex flex-column">
                <!-- Container -->
            <div class="row " >
                <div class="col-12 col-md-4 " id="yellow" style="background:url('<?php the_field('section_background_img'); ?>') no-repeat; background-size:cover;background-position: center top;"> </div>

                <div class="col-12 col-md-8">
                    <div class="bio">               
                            <div class="myhr-vert d-block mx-auto"></div>
                            <h1 class="dark"><?php the_field('titre_section_8'); ?></h1>
                            <h2 class="dark"><?php the_field('sous_titre_section_8'); ?></h2>
                            <div class="myhr d-block mx-auto"></div>
                            <p><?php the_field('texte_section_8'); ?></p>
                            
                    </div>
                </div>
            </div><!-- //row -->
            
         </div><!-- //container -->
    </div>  <!-- //section -->
</div><!-- //fullpage -->


<?php wp_footer(); ?>
</body>
</html>
