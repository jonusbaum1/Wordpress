<?php
/**
 * Template Name: Press
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<!--*FULLPAGE OPTIONT*-->	
	<script type="text/javascript">
		$(document).ready(function() {
			$('#fullpage').fullpage({
				anchors: ['firstPage', 'secondPage', '3rdPage'],
				sectionsColor: ['#ffffff', '#ffffff', '#ffffff'],
				navigation: true,
				navigationPosition: 'right',
				navigationTooltips: ['First page', 'Second page', 'Third and last page'],
				afterResponsive: function(isResponsive){
					
				}

			});
		});
	</script>

<!--*END FULLPAGE OPTIONT*-->
<body>
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'loop-templates/content', 'blank' ); ?>

<?php endwhile; // end of the loop. ?>

<div id="container-fluid">
	<div id="fullpage">
		<div class="section" id="section0">
			<div class="row flex">
				<div class=" col-md-6 text-center"><img class="img-responsive" src="http://lorempixel.com/600/600/food"></div>
				<div class=" col-md-6 text-center">hello world</div>
			</div>
		</div>
		<div class="section" id="section0">
			<div class="row flex">
				<div class=" col-md-6 text-center"><img class="img-responsive" src="http://lorempixel.com/600/600/sports"></div>
				<div class=" col-md-6 text-center">hello world</div>
			</div>
		</div>
		<div class="section" id="section0">
			<div class="row flex">
				<div class=" col-md-6 text-center"><img class="img-responsive" src="http://lorempixel.com/600/600/sports"></div>
				<div class=" col-md-6 text-center">hello world</div>
			</div>
		</div>
		 	
	</div>
</div >


<?php wp_footer(); ?>
</body>
</html>
