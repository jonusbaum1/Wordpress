<?php
/**
 * Template Name: acf
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<!--*FULLPAGE OPTIONT*--> 
<script type="text/javascript">
  $(document).ready(function() {
    $('#fullpage').fullpage({
      anchors: ['firstPage', 'secondPage', '3rdPage', '4th page' ],
      sectionsColor: ['#transparent', 'transparent', '#7E8F7C'],
      navigation: false,
      navigationPosition: 'right',
      navigationTooltips: ['First page', 'Second page', 'Third and last page', '4th page'],
      responsiveWidth: 900,
      afterResponsive: function(isResponsive){

      }

    });
  });
</script>

<!--*END FULLPAGE OPTIONT*-->
<body>
<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'loop-templates/content', 'blank' ); ?>

<?php endwhile; // end of the loop. ?>


<div id="content" class="site-content">
<div id="fullpage">
    
<?php
// check if the repeater field has rows of data
if( have_rows('layout') ):
    
 $sectionID = 0; 
 	// loop through the rows of data
    while ( have_rows('layout') ) : the_row();
          $image1 = get_sub_field('image');
          if( !empty($image1) ): 
              $url = $image1['url'];
          endif;       
        
    $sectionID++;
       ?>
    <div id="section<?php echo $sectionID; ?>" class="section" style="background-image:url(<?phpecho$url;?>) ">
       <h1><?php the_sub_field('anchor'); ?></h1>
        <?php
           // display a sub field value
        the_sub_field('text');        
      
echo '</div>';
    endwhile;

else :
    // no rows found
endif;
?>

</div> <!-- section -->
</div><!-- .site-content -->





<?php wp_footer(); ?>
</body>
</html>
