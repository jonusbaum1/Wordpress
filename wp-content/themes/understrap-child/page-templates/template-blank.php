<?php
/**
 * Template Name: Blank
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package understrap
 */

get_header();
?>
<!--*FULLPAGE OPTIONT*-->	
<script type="text/javascript">
	$(document).ready(function() {
		$('#fullpage').fullpage({
			anchors: ['firstPage', 'secondPage', '3rdPage', '4th page' ],
			sectionsColor: ['#transparent', 'transparent', 'transparent'],
			navigation: false,
			navigationPosition: 'right',
			navigationTooltips: ['First page', 'Second page', 'Third and last page', '4th page'],
			scrollOverflow: true,
			afterResponsive: function(isResponsive){

			}

		});
	});
</script>

<!--*END FULLPAGE OPTIONT*-->
<body>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'loop-templates/content', 'blank' ); ?>

	<?php endwhile; // end of the loop. ?>
	<div id="fullpage">

<!-- SECTION 0  -->
		<?php if( have_rows('slide') ): ?>
			<div class="section " id="section0">

				<?php while( have_rows('slide') ): the_row(); 
				// vars
				$image = get_sub_field('image');
				?>
				<div class="slide" style="background:url(<?php echo $image['url']; ?>)">
					<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
					<span class="titre align-middle"><?php the_sub_field('titre'); ?></span>
					<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
				</div>
				<?php endwhile; ?>
			</div>		
		<?php endif; ?>

<!-- SECTION 1  -->
	<div class="section" id="section1">

		<div class="row">
			<div class="col-sm-12 col-md-4 box1 grayscale" style="background-image:url(<?php the_field('photo_1'); ?>);">
				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
				<span class="soustitre"><?php the_field('titre_1'); ?></span>
				<div class="clair" style="padding:0 50px"><?php the_field('texte_1'); ?></div>
				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
			</div>
			<div class="col-sm-12 col-md-4 box2 grayscale" style="background-image:url(<?php the_field('photo_2'); ?>);">

				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
				<span class="soustitre"><?php the_field('titre_2'); ?></span>
				<div class="clair" style="padding:0 50px"><?php the_field('texte_2'); ?></div>
				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">

			</div>
			<div class="col-sm-12 col-md-4 box3 grayscale" style="background-image:url(<?php the_field('photo_3'); ?>);">

				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
				<span class="soustitre"><?php the_field('titre_3'); ?></span>
				<div class="clair" style="padding:0 50px"><?php the_field('texte_3'); ?></div>
				<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">

			</div>
		</div> <!-- This one wants to be 100% height -->
	</div>

	<!-- SECTION 2  -->
	<div class="section" id="section2" style="background-image:url(<?php the_field('section_2_background_img'); ?>);">
			<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
			<div class="soustitre"><?php the_field('section_2_title'); ?></div>
			<img class="barre" width="1px" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
	</div>

	<!-- SECTION 3  -->
	<div class="section " id="section3" style="background-image:url(<?php the_field('section_3_background_img'); ?>);">
		<div class="slide" id="contact">
			<img class="barre" width="1px" height="50%" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
			<span class="soustitre align-middle">CONTACT</span>
			<p>461 WEST 147 STREET, NEW YORK, NY 10031</br>
			NEW YORK +1 646-709-6261</br>
		KINGSTON +1 876-709-6261</br>
	CHEFGEOFF@GEOFFREYTULLOCH.COM</p>
	<img class="barre" width="1px" height="50%" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
</div>

<div class="slide" id="contact">
	<div class="boxes">
		<div class="box box4">
			<img class="barre" width="1px" height="50%" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
			<span class="soustitre align-middle">CONTACT</span>
			<p>461 WEST 147 STREET, NEW YORK, NY 10031</br>
			NEW YORK +1 646-709-6261</br>
		KINGSTON +1 876-709-6261</br>
	CHEFGEOFF@GEOFFREYTULLOCH.COM</p>
	<img class="barre" width="1px" height="50%" src="<?php echo get_stylesheet_directory_uri(); ?>/img/barre.svg">
</div>
<div class="box box5">
	<span class="titre align-middle">PROJECT TAB FOR CULINARY ROOTS</span>
</div>
</div>
</div>
</div>
<!-- FIN - SECTION 3  -->  	
</div>



<?php wp_footer(); ?>
</body>
</html>
